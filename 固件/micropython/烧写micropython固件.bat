@REM pip install esptool -i https://pypi.tuna.tsinghua.edu.cn/simple
@REM pip install adafruit-ampy -i https://pypi.tuna.tsinghua.edu.cn/simple
@REM esptool.exe -p com6 -b 115200 erase_flash

::进入当前目脚本目录
cd /d %~dp0

::查找是否有esptool安装成功
@echo off
set  findcmd=esptool.py.exe
for %%I in ( %findcmd% ) do IF EXIST %%~$PATH:I ( echo %%~$PATH:I&goto end ) ELSE ( echo %%I is not found in PATH!&goto nopyexe )
:nopyexe
@echo off
set findcmd=esptool.exe
for %%I in ( %findcmd% ) do IF EXIST %%~$PATH:I ( echo %%~$PATH:I&goto end ) ELSE ( echo %%I is not found in PATH!&goto noexe )
:noexe
echo "not find esptool"
pause
exit /b "erro not find esptool.exe or esptool.py.exe"
:end

set app=bootapp-0xe000.bin
set loader=bootloader-0x1000.bin
set partitions=partitiontable-0x8000.bin
set mbin=micropython-0x10000.bin

@REM 只烧写应用分区,不管bootloader和别的分区

%findcmd% erase_flash

TIMEOUT -T 3

%findcmd% --chip esp32 --baud 115200 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0xe000 e-%app% 0x1000 e-%loader% 0x10000 e-%mbin% 0x8000 e-%partitions%

@REM %findcmd% --chip esp32 --baud 115200 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 80m --flash_size detect 0x10000 e-%mbin%

TIMEOUT -T 3

pause
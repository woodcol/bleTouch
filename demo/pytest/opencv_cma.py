#!/usr/bin/env python
# -*- coding: utf-8 -*-
#本代码来自所出售产品的淘宝店店主编写
#未经受权不得复制转发
#淘宝店：https://fengmm521.taobao.com/
#再次感谢你购买本店产品

import cv2
import time
from PyCameraList.camera_device import test_list_cameras, list_video_devices, list_audio_devices
# 调用内置摄像头，所以参数为0，如果有其他的摄像头可以调整参数为1，2


def getCamDeviceWithName(name = 'USB Video'):
    cameras = list_video_devices()
    camdict = dict(cameras)
    print(camdict)
    for k,v in camdict.items():
        if v.find(name) != -1:
            print('used device:%d,%s'%(k,v))
            return k
# deivceID = getCamDeviceWithName('DroidCam Source 3')
deivceID = getCamDeviceWithName('Depstech webcam')
# Depstech webcam
cap = cv2.VideoCapture(deivceID)
img_path = "./screenshot.jpg"
img_path_gray = "./screenshot_gray.jpg"

#!/usr/bin/env python
# coding=utf-8
# cap.set(3, 2048)  # width=1920
# cap.set(4, 1536)  # height=1080
cap.set(cv2.CAP_PROP_FPS,15)
# cap.set(cv2.CAP_PROP_FRAME_WIDTH, 2048)  # width=1920
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1536)  # height=1080
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 3840)  # width=1920
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)  # height=1080
# cap.set(cv2.CAP_PROP_FRAME_WIDTH, 2160)  # width=1920
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 3840)  # height=1080


time.sleep(3)
print('宽:', cap.get(cv2.CAP_PROP_FRAME_WIDTH) )
print('高:', cap.get(cv2.CAP_PROP_FRAME_HEIGHT) )
print('帧率:', cap.get(cv2.CAP_PROP_FPS) )
print('亮度:', cap.get(cv2.CAP_PROP_BRIGHTNESS) )
print('对比度:', cap.get(cv2.CAP_PROP_CONTRAST) )
print('饱和度:', cap.get(cv2.CAP_PROP_SATURATION) )
print('色调:', cap.get(cv2.CAP_PROP_HUE) )
print('曝光度:', cap.get(cv2.CAP_PROP_EXPOSURE) )

cap.set(cv2.CAP_PROP_EXPOSURE, 0)  #曝光度
cap.set(cv2.CAP_PROP_CONTRAST,3)   #对比度

cameras = list_video_devices()
print(dict(cameras))
# #return: {0: 'Intel(R) RealSense(TM) 3D Camera (Front F200) RGB', 1: 'NewTek NDI Video', 2: 'Intel(R) RealSense(TM) 3D Camera Virtual Driver', 3: 'Intel(R) RealSense(TM) 3D Camera (Front F200) Depth', 4: 'OBS-Camera', 5: 'OBS-Camera2', 6: 'OBS-Camera3', 7: 'OBS-Camera4', 8: 'OBS Virtual Camera'}
 
audios = list_audio_devices()
print(dict(audios))
# #return:  {0: '麦克风阵列 (Creative VF0800)', 1: 'OBS-Audio', 2: '线路 (NewTek NDI Audio)'}
 
while True:
    # 从摄像头读取图片
    sucess, img = cap.read()
    img_270_cv_rot = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    down_points = (int(2160/3.8), int(3840/3.8))
    showimg = cv2.resize(img_270_cv_rot, down_points, interpolation= cv2.INTER_LINEAR)
    # print(sucess)
    # print(sucess,img,img.shap[1],img.shap[0])
    # 转为灰度图片s
    
    # 显示摄像头
    cv2.imshow('----------please enter "s" to take a picture----------', img)
    # 保持画面的持续,无限期等待输入
    k = cv2.waitKey(1)
    # k == 27 通过esc键退出摄像 ESC(ASCII码为27)
    if k == 27:
        cv2.destroyAllWindows()
        break
    elif k == ord("s"):
        # 通过s键保存图片，并退出。
        cv2.imwrite(img_path, img)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(img_path_gray, gray)
        cv2.destroyAllWindows()
        break
    elif k == ord("a"):
        print(img.shape[:2])
# 关闭摄像头
cap.release()

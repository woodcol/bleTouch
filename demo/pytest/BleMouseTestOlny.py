#!/usr/bin/env python
# -*- coding: utf-8 -*-
#本代码来自所出售产品的淘宝店店主编写
#未经受权不得复制转发
#淘宝店：https://fengmm521.taobao.com/
#再次感谢你购买本店产品
from ast import Try
import os,sys
import serial
import time
import json
import math

try:
    import ch340usb
except Exception as e:
    pass

from sys import version_info  

isTest = False


MAX_X = 25400   #最大逻辑x坐标
MAX_Y = 15875   #最大逻辑y坐标

MAX_T = 8191    #最大按压力度

W = 1024        #定义手机屏宽度
H = 768         #定义手机屏高度

wscal = float(MAX_X)/float(W)
hscal = float(MAX_Y)/float(H)

LEFTDOWN = '21'     #左键按下
RIGHTDOWN = '23'    #右键按下
ALLUP     = '20'    #按键抬起

REFLASHTIME = 8     #鼠标刷新时间,单位:ms

def conventPoint(p):
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    return (x,y)

def leftDown(p):
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    out = '[%d,%d,1]'%(x,y)
    return out

def rightDown(p):
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    out = '[%d,%d,2]'%(x,y)
    return out

def click(p):
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    out = '[%d,%d,3]'%(x,y)
    return out

def allUp(p):
    x = int(p[0] * wscal)
    y = int( p[1] * hscal)
    out = '[%d,%d,0]'%(x,y)
    return out
# 已知平面上有两个不复合的点p1(x1,y1),P2(x2,y2).现在要求物体从p1以速度v移动到p2,用python写一个函数,输入时间t,计算并返回当前物体坐标p(x,y)
#从p1滑动到p2,speed:每秒移动的像素




def move(p1,p2,speed = 800.0):
    psp = speed * wscal
    tp1 = conventPoint(p1)
    tp2 = conventPoint(p2)
    out = '{%d,%d,%d,%d,%d}'%(tp1[0],tp1[1],tp2[0],tp2[1],psp)
    return out
#'@'工作模式字典
# type2Pins = {1:['0','1'],2:['2','3'],3:['4','5'],4:['6','7'],5:['8','9'],6:['a','b'],7:['c','d'],8:['e','f'],9:['g','h'],10:['i','j'],11:['k','l'],12:['m','n'],13:['o','p'],14:['q','r'],15:['s','t'],16:['u','v']}
type2Pins = {1:['<FFFE>','<FFFF>'],2:['<FFFD>','<FFFF>'],3:['<FFFB>','<FFFF>'],4:['<FFF7>','<FFFF>'],5:['<FFEF>','<FFFF>'],6:['<FFDF>','<FFFF>'],7:['<FFBF>','<FFFF>'],8:['<FF7F>','<FFFF>'],9:['<FEFF>','<FFFF>'],10:['<FDFF>','<FFFF>'],11:['<FBFF>','<FFFF>'],12:['<F7FF>','<FFFF>'],13:['<EFFF>','<FFFF>'],14:['<DFFF>','<FFFF>'],15:['<BFFF>','<FFFF>'],16:['<7FFF>','<FFFF>']}


serialDelay = 0.02

#获取当前python版本
def pythonVersion():
    return version_info.major


def readcom(t):
    n = t.inWaiting()
    while n<=0:
        time.sleep(serialDelay)
        n = t.inWaiting()
    pstr = t.read(n)
    if pythonVersion() > 2:
        print(pstr.decode())
    else:
        print(pstr)
    

def sendcmd(t,cmd):
    sendstr = cmd
    # if cmd[-1] != '\r':
    #     sendstr += '\r'
    print(sendstr)
    if pythonVersion() > 2:
        s = t.write(sendstr.encode())
    else:
        s = t.write(sendstr.encode())
    t.flush()

def sendAndread(t,v):
    if isTest:
        f = open('test.txt','a')
        f.write(v + '\n')
        f.close()
    else:
        sendcmd(t,v)
        time.sleep(serialDelay)
        readcom(t)


def getSerialConfig():
    f = open('config.txt','r')
    dat = f.read()
    f.close()
    tmpdict = json.loads(dat)
    return tmpdict

def getPinDat(p):
    return type2Pins[p]

def runcom():
    
    conf = getSerialConfig()
    dev = conf['port']
    btv = conf['btv']
    t = serial.Serial(dev,btv,timeout=1)
    if t:
        print(t.name)               #串口名
        print(t.port)               #串口号
        print(t.baudrate)           #波特率
        print(t.bytesize)           #字节大小
        print(t.parity)             #校验位N－无校验，E－偶校验，O－奇校验
        print(t.stopbits)           #停止位
        print(t.timeout)            #读超时设置
        print(t.writeTimeout)       #写超时
        print(t.xonxoff)            #软件流控
        print(t.rtscts)             #硬件流控
        print(t.dsrdtr)             #硬件流控
        print(t.interCharTimeout)   #字符间隔超时
        print('-'*10)
        time.sleep(1)

        for i in range(2):
            print(2-i)
            time.sleep(1)
        # for tmp in range(3):
        #     for i in range(16):
        #         v = getPinDat(i+1)
        #         td = v[0]
        #         tu = v[1]
        #         sendAndread(t, td)
        #         time.sleep(0.1)
        #         sendAndread(t, tu)
        #         time.sleep(0.1)
        # sendAndread(t, '#')
        for tmp in range(1):
            sendAndread(t, '<0000>')
            time.sleep(0.3)
            sendAndread(t, '<FFFF>')
            time.sleep(0.3)
        time.sleep(1)
        for i in range(1,10):
            xtmp = 1024*i/10.0
            ytmp = 1024*i/10.0
            ltstr = allUp((xtmp,ytmp))
            sendAndread(t, ltstr)
            time.sleep(0.5)
        time.sleep(1)
        tspx = 1024/2
        tspy = 768/6 
        tepx = 1024/2
        tepy = 768*5/6.0
        p1 = (tspx,tspy)
        p2 = (tepx,tepy)
        mstr = move(p1,p2,2000)
        sendAndread(t,mstr)
        time.sleep(0.005)
        time.sleep(1)
        ltstr = allUp((0,0))
        sendAndread(t, ltstr)
        t.close()
    else:
        print('串口不存在')

def main():
    runcom()
def test():
    move((0,0),(800,800))
if __name__ == '__main__':
    main()
    # test()
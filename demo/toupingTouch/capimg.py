
import cv2
import os,sys
import subprocess
import numpy
import time
# from PyCameraList.camera_device import test_list_cameras, list_video_devices, list_audio_devices
#摄像头列表库:pip install pycameralist
from PyCameraList.camera_device import list_video_devices



# 执行命令
def cmd(cmdStr: str):
    cmds = cmdStr.split(' ')
    proc = subprocess.Popen(
        cmds,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = proc.communicate()
    # returncode 代表执行cmd命令报错
    if proc.returncode > 0:
        print('cmd erro')
        raise Exception(proc.returncode, stderr)
    return stdout

#方案一
def screencap1():
    cmd('./adbtool/adb.exe shell screencap -p > /sdcard/screen.png')
    cmd('./adbtool/adb.exe pull /sdcard/screen.png ./screen.png')
    return cv2.imread('./screen.png')

def screencap2():
    byteImage = cmd('./adbtool/adb.exe shell screencap -p').replace(b'\r\n', b'\n')
    # opencv读取内存图片
    return cv2.imdecode(numpy.asarray(bytearray(byteImage), dtype=numpy.uint8), cv2.IMREAD_COLOR)

def getCamDeviceWithName(name = 'USB Video'):
    cameras = list_video_devices()
    camdict = dict(cameras)
    print(camdict)
    for k,v in camdict.items():
        if v.find(name) != -1:
            print('used device:%d,%s'%(k,v))
            return k

def showDeiveces():
    cameras = list_video_devices()
    camdict = dict(cameras)
    print(camdict)
    # for k,v in camdict.items():
    #     if v.find(name) != -1:
    #         print('used device:%d,%s'%(k,v))
    #         return k

cap = None
#通过摄像头获取图片
def camImg(camname = 'Depstech webcam'):
    global cap
    if not cap:
        showDeiveces()
        # deivceID = getCamDeviceWithName('DroidCam Source 3')
        deviceID = getCamDeviceWithName(camname)
        # deviceID = 1
        # cap = cv2.VideoCapture(deviceID,cv2.CAP_DSHOW)
        cap = cv2.VideoCapture(deviceID)
        print(1)
        # cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
        cap.set(6 ,cv2.VideoWriter_fourcc('M', 'J', 'P', 'G') )
        # cap.set(cv2.CAP_PROP_FPS,15)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)  # width=1920
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)  # height=1080
        cap.set(cv2.CAP_PROP_FPS,120)
        # cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)  # width=1920
        # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)  # height=1080
        print(3)
        # time.sleep(3)
        # cap.set(cv2.CAP_PROP_EXPOSURE,0)  #曝光度
        # print(4)
        # cap.set(cv2.CAP_PROP_CONTRAST,3)   #对比度
        # cap.set(cv2.CAP_PROP_FPS,120)
        # print(5)
        print('宽:', cap.get(cv2.CAP_PROP_FRAME_WIDTH) )
        print('高:', cap.get(cv2.CAP_PROP_FRAME_HEIGHT) )
        print('帧率:', cap.get(cv2.CAP_PROP_FPS) )
        print('亮度:', cap.get(cv2.CAP_PROP_BRIGHTNESS) )
        print('对比度:', cap.get(cv2.CAP_PROP_CONTRAST) )
        print('饱和度:', cap.get(cv2.CAP_PROP_SATURATION) )
        print('色调:', cap.get(cv2.CAP_PROP_HUE) )
        print('曝光度:', cap.get(cv2.CAP_PROP_EXPOSURE) )
        
    sucess, frame = cap.read()
    # target_size = (1080,1920)
    # resized_frame  =  cv2.resize(frame, target_size, interpolation=cv2.INTER_LINEAR)
    return sucess,frame.copy()

def test():
    winname = 'captest'
    cv2.namedWindow(winname)
    img = screencap2()
    # print(type(img))
    down_points = (int(img.shape[1]/2), int(img.shape[0]/2))
    img = cv2.resize(img, down_points, interpolation= cv2.INTER_LINEAR)
    imgSize = (int(img.shape[1]),int(img.shape[0]))
    cv2.imshow(winname, img)
    key = cv2.waitKey(0)

def test2():
    #adb调用获取截图数据流， ps: 127.0.0.1:7555 为手机的端口号，这里我使用模拟器来测试
    process = subprocess.Popen(
        ['adb', 'shell', 'screencap', '-p'],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout = process.stdout.read()
    stdout = stdout.replace(b'\r\n', b'\n')
    #保存图片
    with open('./screnn2.png', 'wb') as f:
        a = f.write(stdout)

if __name__ == '__main__':  
    # test2()
    test()
    
# ————————————————
# 版权声明：本文为CSDN博主「QQQQQYE」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
# 原文链接：https://blog.csdn.net/qinye101/article/details/120000500
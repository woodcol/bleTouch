import cv2
import requests
import numpy as np
import time
import os
import capimg
class eventDataObj():
    def __init__(self,pdat):
        self.dat = pdat.copy()

lineColor = (0,0,255)

class cv2Stream():  
    def __init__(self,pqueue,imgpth,pressMFunc,releaseMFunc,moveMfunc,preaseKFunc,imgSizeFunc,imgSize,pimgResize = 2.5):  
        self.queue = pqueue                 #返回动作事件的列队
        self.mPressFunc = pressMFunc        #鼠标左键按下事件
        self.mReleaseFunc = releaseMFunc    #鼠标左键抬起事件
        self.mMoveFunc = moveMfunc          #鼠标移动事件
        self.kPressFunc = preaseKFunc       #键盘事件
        self.imgSizeChangeFUnc = imgSizeFunc
        self.baseSize = imgSize             #手机画面大小
        self.imgReSize = pimgResize         #手机画面显示缩小比例
        self.pth = imgpth
        self.winname = 'fengmm521.taobao.com'
        self.saveimgPth = 'saveimg'
        if not os.path.exists(self.saveimgPth):
            os.mkdir(self.saveimgPth)
        self.onSaveImg = False
        self.img = None
        self.baseImg = None
        cv2.namedWindow(self.winname)
        cv2.setMouseCallback(self.winname, self.on_EVENT_LBUTTONDOWN)
        self.cImg = None
        self.isCopyImg = False
        self.imgSize = None
        self.runObj = None

        self.isChangeImg = False
        self.changeIngPth = None
        self.initCV2()
    def initCV2(self):
        # print(self.pth)
        # self.baseImg = cv2.imread(self.pth)
        ret,self.baseImg = capimg.camImg()
        print('base img size:',self.baseImg.shape[:2])
        self.baseImg = self.rotateImg(self.baseImg)
        print('base img size2:',self.baseImg.shape[:2])
        down_points = (int(self.baseSize[0]/self.imgReSize), int(self.baseSize[1]/self.imgReSize))
        print('dp size:',down_points)
        self.img = cv2.resize(self.baseImg, down_points, interpolation= cv2.INTER_LINEAR)
        print('img size:',self.img.shape[:2])
        self.imgSize = down_points
        print(self.imgSize)
        print(self.img.shape[:2])
        self.onImgSizeChange()
    def updateImg(self):
        res,self.baseImg = capimg.camImg()
        
        self.baseImg = self.rotateImg(self.baseImg)
        down_points = (int(self.baseSize[0]/self.imgReSize), int(self.baseSize[1]/self.imgReSize))
        self.img = cv2.resize(self.baseImg, down_points, interpolation= cv2.INTER_LINEAR)
    def initSize(self):
        self.img = cv2.resize(self.img, self.imgSize, interpolation= cv2.INTER_LINEAR)
    #鼠标事件
    def on_EVENT_LBUTTONDOWN(self,event, x, y,flags, param):
        # self.mPressFunc = pressMFunc        #鼠标左键按下事件
        # self.mReleaseFunc = releaseMFunc    #鼠标左键抬起事件
        # self.kPressFunc = preaseKFunc       #键盘按钮按下事件
        # self.mMoveFunc = moveMfunc          #鼠标移动事件
        #左键按下
        if event == cv2.EVENT_LBUTTONDOWN:
            self.mPressFunc(x,y,flags)
        #左键抬起
        elif event == cv2.EVENT_LBUTTONUP:
            self.mReleaseFunc(x,y,flags)
        elif event == cv2.EVENT_MOUSEMOVE:
            self.mMoveFunc(x,y,flags)
    def on_EVENT_KeyDown(self,pkey):
        self.kPressFunc(self,pkey)
    def saveLastImg(self):
        spth = self.saveimgPth + os.sep + str(int(time.time())) + '.jpg'
        print(spth)
        # down_points = (int(self.baseSize[0]), int(self.baseSize[1]))
        # timg =  cv2.resize(self.baseImg, down_points, interpolation= cv2.INTER_LINEAR)
        cv2.imwrite(spth, self.baseImg)
    def copyImg(self,rObj):
        self.runObj = rObj
        self.isCopyImg = True
    def ocrImg(self):
        self.onOCRImg = True
    def rotateImg(self,pimg):
        img_270_cv_rot = cv2.rotate(pimg, cv2.ROTATE_90_CLOCKWISE)
        return img_270_cv_rot

        height, width = pimg.shape[:2]
        center = (width/2, height/2)
        rotate_matrix = cv2.getRotationMatrix2D(center=center, angle=-90, scale=1)
        rotateimg = cv2.warpAffine(src=pimg, M=rotate_matrix, dsize=(width, height))
        return rotateimg
    def getOcrImg(self):
        return self.baseImg
    def onImgSizeChange(self):
        # self.imgChangeFunc(self.imgSize)
        print('img size change')
        self.imgSizeChangeFUnc(self.imgSize)
    def saveImgToFile(self,img):
        spth = self.saveimgPth + os.sep + str(int(time.time())) + '.jpg'
        print(spth)
        cv2.imwrite(spth, img)
    def showAdbImg(self):
        self.img = capimg.screencap2()
        self.initSize()
    def showFile(self,imgPth):
        print(imgPth)
        self.img = cv2.imread(imgPth)
        self.initSize()
    def start(self):
        while True:
            # if self.isChangeImg and self.cImg.any:
            #     self.img = self.cImg.copy()
            #     self.initSize()
            #     self.cImg = None
            self.updateImg()
            cv2.imshow(self.winname, self.img)
            key = cv2.waitKey(1)
            if key > 0:
                self.on_EVENT_KeyDown(key)

def main():
    def mouseFunc(p):
        print(p)
    def keyFunc(k):
        print('key down:%d'%(k))
        if k & 0xFF == 0x1B:#如果是ESC键接下,则退出
            exit(0)
    while True:
        time.sleep(0.01)

if __name__ == '__main__':  
    main()
    
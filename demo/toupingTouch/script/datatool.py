#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import os

MAPPTH = 'script' + os.sep  + 'data' + os.sep + 'map.txt'
TEAMPTH = 'script' + os.sep  + 'data' + os.sep +'team.txt'
UIPTH = 'script' + os.sep  + 'data' + os.sep +'ui.txt'

#加载文件,按行加载字符串
def loadFile(pth):
    f = open(pth,'rb')
    dat = f.read()
    f.close()
    lines = dat.decode().split('\n')
    out = []
    for i,v in enumerate(lines):
        tmp = v.replace('\r','').replace('\n','')
        out.append(tmp)
    return out
#地图坐标
#示例:地牢:468,164,9-1,c
#地图名,x坐标,y坐标,地图编号,地图类型(l:世界地图左边时坐标,r:世界地图右边时坐标,c,地图中不显示地图的了地图)
def mapData():
    mapls = loadFile(MAPPTH)
    mapNameDat = {}
    mapIDDat = {}
    for i,v in enumerate(mapls):
        tmp = v.split(',')
        # print(tmp)
        print(tmp)
        mapNameDat[tmp[0]] = {'p':(int(tmp[1]),int(tmp[2])),'id':tmp[3],'type':tmp[4]}
        mapIDDat[tmp[3]] = {'p':(int(tmp[1]),int(tmp[2])),'name':tmp[0],'type':tmp[4]}
    return mapNameDat,mapIDDat

#界面坐标
#worldmap,27,37,打开世界地图
def uiData():
    uils = loadFile(UIPTH)
    uidat = {}
    for i,v in enumerate(uils):
        tmp = v.split(',')
        uidat[tmp[0]] = {'p':(int(tmp[1]),int(tmp[2])),'s':tmp[3]}
    return uidat

def main():
    mapdat = mapData()
    teamdat = teamData()
    uidat = uiData()
    print(uidat)
if __name__ == '__main__':
    main()

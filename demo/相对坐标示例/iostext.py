from ast import Try
import os,sys
import serial
import time
import json

serialDelay = 0.02

def uartsend(cmd):
    if type(cmd) == str:
        cmd = cmd.encode()
    t.write(cmd)
    t.flush()
    time.sleep(0.005)

def sendAndRead(cmd):
    # sendAndread
    if type(cmd) == str:
        cmd = cmd.encode()
    t.write(cmd)
    t.flush()
    time.sleep(serialDelay)

    # readcom
    n = t.inWaiting()
    while n<=0:
        time.sleep(serialDelay)
        n = t.inWaiting()
    pstr = t.read(n).decode()
    print(f'readcom = {pstr}')


port = 'com4'
btv = 115200
t = serial.Serial(port,btv,timeout=1)

print(f'串口名        {t.name}\n')             
print(f'串口号        {t.port}')             
print(f'波特率        {t.baudrate}')         
print(f'字节大小       {t.bytesize}')       
print(f'校验位N       {t.parity} －无校验，E－偶校验，O－奇校验')        
print(f'停止位        {t.stopbits}')          
print(f'读超时设置      {t.timeout}')       
print(f'写超时        {t.writeTimeout}')      
print(f'软件流控       {t.xonxoff}')         
print(f'硬件流控       {t.rtscts}')          
print(f'硬件流控       {t.dsrdtr}')          
print(f'字符间隔超时     {t.interCharTimeout}')  
print('-'*10)
time.sleep(1)


# https://www.rapidtables.com/convert/number/hex-to-decimal.html
# https://www.rapidtables.com/convert/number/decimal-to-hex.html

# 格式: [9个Bytes]
#   B1    B2 B3                     B4 B5         B6    B7     B8 B9
#   抬起   x方向 (-126-126) & 速度    y..          滚轮x  滚轮y   无用
#   00    0f 01                     00 00         00    00     0000

# 问题1,  连接一段时间后, 设备与苹果手机断开连接, 蓝牙设备里点击连接无效, 需要对设备断电再插上电脑

# 问题2,  向右移动10,再向左移动10, 成功回到原点. 但是接下来的点击命令, 造成了意外的鼠标移动
#   问题原因 - 以下点击/抬起操作,似乎会重复最后一次移动命令 也就是向左移10
#   readcom = 010000000000000000 点击 
#   readcom = 000000000000000000 抬起

import struct


# hex_value = struct.pack('b', -10).hex()

# print(hex_value)
# 将-10转换为一个字节的16进制表示
def getHexWithNum(p):
    hex_value = struct.pack('b', p).hex()
    # print(type(hex_value),hex_value)
    return hex_value

# d = getHexWithNum(-10)
# print(d)

def delayTime(m,s):
    absm = abs(m)
    dt = (absm*2*8 + s*8)/1000.0
    return dt
    

def cmdmove(m,step):
    mstr = getHexWithNum(m)
    stepstr = getHexWithNum(step)
    return mstr,stepstr

def movex(m,s):
    tmpm,tmps = cmdmove(m,s)
    cmd = '[00' + tmpm + tmps + '00' + '00'+ '00' + '00' + '0000]'
    sendAndRead(cmd)
    dt = delayTime(m,s)
    time.sleep(dt)

def send(b=0,x=0,y=0,h=0,w=0):
    cmd = '['
    if b:
        cmd += '01'
    else:
        cmd += '00'
    if x <= -120 or x >= 120:
        print('x erro')
        return
    else:
        dx = getHexWithNum(x)
        cmd += dx
    if y <= -120 or y >= 120:
        print('y erro')
        return
    else:
        dy = getHexWithNum(y)
        cmd += dy
    cmd += '0000]'
    uartsend(cmd)
    

def movey(m,s):
    tmpm,tmps = cmdmove(m,s)
    cmd = '[00' + '00' + '00' + tmpm + tmps + '00' + '00' + '0000]'
    sendAndRead(cmd)
    dt = delayTime(m,s)
    time.sleep(dt)

def home():
    movex(-50,20)
    movey(-50,20)

def home2():
    for i in range(100):
        send(0,-20,-20)
    print('in home')

def press():
    cmd = ('[01' + '0000' + '0000' + '0000' + '0000]')# 点击 
    sendAndRead(cmd)
def release():
    cmd = ('[00' + '0000' + '0000' + '0000' + '0000]') # 抬起
    sendAndRead(cmd)

def test1():
    home()
    movex(10,1)
    movey(10,1)
def test2():
    home2()
    for i in range(200):
        send(0,2,2)
test2()
time.sleep(1)
t.close()


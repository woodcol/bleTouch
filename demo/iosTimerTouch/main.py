#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author:洪卫
 

import os,sys
import serial
import time
import json
import timetool
from sys import version_info  
import bleTouch
def pythonVersion():
    return version_info.major

dtime = 0.1
tcount = 10
tdelay = 0.3



def getTime():
    global dtime,tdelay,tcount
    f = open('timer.txt','r')
    dat = f.read()
    f.close()
    configs = json.loads(dat)
    ttime = int(timetool.conventTimeFromStrConfig(configs['time'])*1000)
    dtime = float(configs['tdtime']/1000.0)
    if dtime > 0.02:
        dtime -=0.02
    tcount = configs['tCount']
    tdelay = float(configs['delay']/1000.0)
    print('time1:%s'%(configs['time']))
    print(ttime,dtime,tcount)
    return ttime

def main():
    ttime = getTime()
    isRuned = False
    bleTouch.goPoint1()
    while True:
        time.sleep(0.002) #进行一个延时,防止占用CPU过高
        nowtime = int(time.time()*1000)
        if ttime <= nowtime:
            if not isRuned and ttime <= nowtime:
                bleTouch.run(tcount,dtime,tdelay)
                isRuned = True
        elif isRuned: #定时器已经运行
            print('run end ...')
            return
            
    
if __name__ == '__main__':
    main()

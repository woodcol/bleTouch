import time
import serial

dev = 'com51' #这里修改串口号
btv = 115200       #串口波特率
t = None

def sendCmd(cmd):
        sendstr = cmd
        print(sendstr)
        s = t.write(sendstr.encode())
        t.flush()

def initSerial():
    global t
    t = serial.Serial(dev,btv,timeout=1)
    if t:
        print(t.name)               #串口名
        print(t.port)               #串口号
        print(t.baudrate)           #波特率
        print(t.bytesize)           #字节大小
        print(t.parity)             #校验位N－无校验，E－偶校验，O－奇校验
        print(t.stopbits)           #停止位
        print(t.timeout)            #读超时设置
        print(t.writeTimeout)       #写超时
        print(t.xonxoff)            #软件流控
        print(t.rtscts)             #硬件流控
        print(t.dsrdtr)             #硬件流控
        print(t.interCharTimeout)   #字符间隔超时
        print('-'*10)
        time.sleep(1)
        sendCmd('#')


    #https://www.cnblogs.com/engpj/p/16933904.html
def recv_from_serial():
        """
        接收串口返回信息
        """
        serialDelay = 0.001
        while True:
            n = t.inWaiting()
            while n<=0:
                time.sleep(serialDelay)
                n = t.inWaiting()
            pstr = t.read(n)
            print(str(pstr))
#!/usr/bin/python
# -*- coding: utf-8 -*-
#创建SocketServerTCP服务器：
import time

LEFTDOWN = '21'     #左键按下
RIGHTDOWN = '23'    #右键按下
ALLUP     = '20'    #按键抬起

class _eventObj():
    def __init__(self,dtype,pdat):
        self.tp = dtype   #接收到的数据类型,1为鼠标坐标,2为键盘按下值
        self.dat = pdat

que = None


MAX_X = 25400   #鼠标最大逻辑x坐标
MAX_Y = 15875   #鼠标最大逻辑y坐标

class PointUtil(object):
    """docstring for Stat"""
    def __init__(self,pwh):
        self.w = pwh[0]      #手机宽像素
        self.h = pwh[1]      #手机高像素值
        self.LW = MAX_X      #点击器逻辑坐标宽度
        self.LH = MAX_Y      #点击器逻辑坐标高度
        self.dot = (float(self.LW)/float(self.w),float(self.LH)/float(self.h))
    def getLP(self,p):
        x = p[0]*self.dot[0]
        y = p[1]*self.dot[1]
        return (x,y)

POINTUTIL = None


    # time.sleep(0.001) #延时10ms
def initCmdUtil(q,pwh):
    global que,POINTUTIL
    que = q
    POINTUTIL = PointUtil(pwh)

def sendCmd(cmd):
    eobj = _eventObj(None,cmd)
    que.put(eobj)

def release(x,y):
    if not que:
        print('erro:not init cmd Queue')
    tmpp = POINTUTIL.getLP((x,y))
    tcmd = '[%d,%d,0]'%(tmpp[0],tmpp[1])
    sendCmd(tcmd)
    return tcmd + ':' + str(time.time())
def press(x,y):
    if not que:
        print('erro:not init cmd Queue')
    tmpp = POINTUTIL.getLP((x,y))
    tcmd = '[%d,%d,1]'%(tmpp[0],tmpp[1])
    sendCmd(tcmd)
    return tcmd + ':' + str(time.time())
def click(x,y):
    pcmd = press(x,y)
    pcmd = pcmd + ':' + str(time.time())
    time.sleep(0.06)
    rcmd = release(x,y)
    rcmd = rcmd + ':' + str(time.time())
    return '%s\n%s'%(pcmd,rcmd)

#按下点击笔并滑动到指定坐标x,y,然后抬起点击笔,l为按下移动的跑离,(x,y)按下后要移动到的坐标
def touchMoveTo(p1,p2,speed = 10000):
    if not que:
        print('erro:not init cmd Queue')
    tmp1 = POINTUTIL.getLP(p1)
    tmp2 = POINTUTIL.getLP(p2)
    tcmd = '{%d,%d,%d,%d,%d}'%(tmp1[0],tmp1[1],tmp2[0],tmp2[1],speed)
    sendCmd(tcmd)
    return tcmd + ':' + str(time.time())

#主函数,程序从这里开始运行
def main():
    pass
    
if __name__ == '__main__':
    main()
    # test()



# Ble蓝牙键盘鼠标和wifi版16头点击器

## 介绍
新款的带有蓝牙5.2可以模拟蓝牙OTG鼠标和蓝牙键盘的点击器,是之前的16头wifi版击器加强版,本产品使用ESP32-WROOM-32d模块,是上一代wifi版点击器的加强版,除了支持上一代wifi版点击器的所有功能外,还支持模拟蓝牙键盘和蓝牙鼠标,并且键盘和鼠标为免驱动支持手机安卓,iOS和电脑的windows,linux,Mac os系统.

## 如何获取硬件

硬件唯一购买地址:
https://item.taobao.com/item.htm?id=722805535129

## 功能介绍

##### 一.基础功能:

1.支持PC端通过wifi控制16个外部物理点击头点击
2.支持PC端通过USB的串口控制外部物理点击头点击
3.支持micropython程序直接在主控板上脱机运行控制16个外部物理点击头点击

以上三种控制方式都和上一代产品一样提供源码

##### 二.新增功能:

新增通过串口控制物理点击头点击电容屏的同时,还支持通过串口模拟蓝牙键盘和蓝牙鼠标进行按键输入和鼠标点击

因为主控芯片原因,主控板无法同时支持蓝牙和wifi,所以当使用蓝牙鼠标和蓝牙键盘功能只能过通过电脑USB串口控制模拟鼠标和键盘动作

micropython版固件的蓝牙键盘蓝牙鼠标目前还在开发中,故暂时不支持脱机使用蓝牙键盘和蓝牙鼠标功能


#### 安装教程

上位机程序使用python开发,所以要使用本点击器需要上位机电脑安装python才能运行上位机的示例程序.建议安装python3.8及以上版本python.我们所提供的python示例程序都是在python3.8版本下开发,理论上python3.8的版本也支持,但更高版本的python我们没有对基进行过测试,有问题可以联系我

安装好python后,还需要安装python的串口库,安装好python后可以使用pip进行安装
``` bash
pip install pyserial -i https://pypi.tuna.tsinghua.edu.cn/simple

```

如果要使用令命行烧写esp32和esp8266固件还需要安装esptool的python工具:
``` bash
pip install esptool -i https://pypi.tuna.tsinghua.edu.cn/simple

```

python和其他调试工具,以及代码编辑器vscode获取网盘地址:

链接: https://pan.baidu.com/s/1Izdy5mgndu-9RsO-Y46McA?pwd=1234 提取码: 1234

USB转串口USB驱动(mac os 10.14及以上版本系统自代驱动,win7和win10系统可能需要安装USB驱动):

链接: https://pan.baidu.com/s/120bRP7EKF-oraQcdhxzfbQ?pwd=1234 提取码: 1234 


#### 使用说明

这里只说明一下USB串口发送指令控制鼠标,键盘和物理点击头的方法,本蓝牙wifi板16头点击器默认发货烧写这个固件:

##### 蓝牙绝对鼠标串口控制指令

```python
[x,y,1]
```
上边的指令是在[ ]中间的字符串分为3个部分,下边进行说明
 **x**  | **y** | **1** 
---------|----------|----------
鼠标x坐标    | 鼠标y坐标    | 点击状态   
 0~25400  | 0~15875  | 0,1,2,3  

坐标值说明:
x坐标在最左边的值为0,最右边的值为25400,坐标和手机实际物理坐标是线性关系,即x坐标在屏中间时值为(25400/2)
y坐标同x坐标类似,即y坐标在最上边时值为0,在最下边是值为15875,实际物理坐标和这个值是线性关系

后边的点击状态说明:
0:抬起状态
1:左键按下
2:右键按下
3:左键点击一下

使用串口发送指令时要包含[]方括号,中间不能有空格.例如:

```pythoh
[5000,5000,1]
```
表示鼠标在5000,5000这个坐标按下

##### 1.鼠标在(0,0)按下
发送在(0,0)坐标下鼠标按下,按下力度511,指令如下
```python
[0,0,1]
```

##### 2.鼠标在(0.0)抬起
```python
[0,0,0]
```

##### 3.鼠标在(0,0)坐标点击一下

```python
[0,0,3]
```

##### 4.鼠标右键按下
```python
[0,0,2]
```
右键按下只在电脑上起作用,手机上还是相当于左键

### 鼠标的滑动操作

鼠标滑动有两个方式来控制,
1)使用上边的按下指令发送一整个滑动轨迹即可
2)使用下边单独的滑动指令

```python
{x1,y1,x2,y2,speed}
```
这个指令说明,这是一个按下滑动,或者叫托动也可以:
x1,y1:为滑动起点坐标
x2,y2:为滑动终点坐标
speed:为滑动的速度,单位是每秒移动值

注意串口发送时两边的 {} 花括号不能省.

##### 蓝牙键盘控制指令
表示一个按键按下
```python
#键盘字符串或者单个字符的ASCII码+按键状态
(+键盘按键的16进制字节码)
```
表示一个按键抬起
```
(-键盘按键的16进制字节码)
```
表示一个按键被点击一次
```
(~键盘按键的16进制字节码)
```
媒体按键按下
```
(@后跟两个字节的媒体字节码)
```
媒体按键抬起
```
(^后跟两个字节的媒体字节码)
```

所以字节码都为16进制的字符串表示,()圆括号也要在串口中发送,括号是区分指令是鼠标还是键盘按键的标志,所以发送时不能少

下边是电脑键盘和媒体按键值的说明

``` Java
//键盘动作

// const uint8_t KEY_LEFT_CTRL = 0x80;
// const uint8_t KEY_LEFT_SHIFT = 0x81;
// const uint8_t KEY_LEFT_ALT = 0x82;
// const uint8_t KEY_LEFT_GUI = 0x83;
// const uint8_t KEY_RIGHT_CTRL = 0x84;
// const uint8_t KEY_RIGHT_SHIFT = 0x85;
// const uint8_t KEY_RIGHT_ALT = 0x86;
// const uint8_t KEY_RIGHT_GUI = 0x87;

// const uint8_t KEY_UP_ARROW = 0xDA;
// const uint8_t KEY_DOWN_ARROW = 0xD9;
// const uint8_t KEY_LEFT_ARROW = 0xD8;
// const uint8_t KEY_RIGHT_ARROW = 0xD7;
// const uint8_t KEY_BACKSPACE = 0xB2;
// const uint8_t KEY_TAB = 0xB3;
// const uint8_t KEY_RETURN = 0xB0;
// const uint8_t KEY_ESC = 0xB1;
// const uint8_t KEY_INSERT = 0xD1;
// const uint8_t KEY_DELETE = 0xD4;
// const uint8_t KEY_PAGE_UP = 0xD3;
// const uint8_t KEY_PAGE_DOWN = 0xD6;
// const uint8_t KEY_HOME = 0xD2;
// const uint8_t KEY_END = 0xD5;
// const uint8_t KEY_CAPS_LOCK = 0xC1;
// const uint8_t KEY_F1 = 0xC2;
// const uint8_t KEY_F2 = 0xC3;
// const uint8_t KEY_F3 = 0xC4;
// const uint8_t KEY_F4 = 0xC5;
// const uint8_t KEY_F5 = 0xC6;
// const uint8_t KEY_F6 = 0xC7;
// const uint8_t KEY_F7 = 0xC8;
// const uint8_t KEY_F8 = 0xC9;
// const uint8_t KEY_F9 = 0xCA;
// const uint8_t KEY_F10 = 0xCB;
// const uint8_t KEY_F11 = 0xCC;
// const uint8_t KEY_F12 = 0xCD;
// const uint8_t KEY_F13 = 0xF0;
// const uint8_t KEY_F14 = 0xF1;
// const uint8_t KEY_F15 = 0xF2;
// const uint8_t KEY_F16 = 0xF3;
// const uint8_t KEY_F17 = 0xF4;
// const uint8_t KEY_F18 = 0xF5;
// const uint8_t KEY_F19 = 0xF6;
// const uint8_t KEY_F20 = 0xF7;
// const uint8_t KEY_F21 = 0xF8;
// const uint8_t KEY_F22 = 0xF9;
// const uint8_t KEY_F23 = 0xFA;
// const uint8_t KEY_F24 = 0xFB;

// typedef uint8_t MediaKeyReport[2];

// const MediaKeyReport KEY_MEDIA_NEXT_TRACK = {1, 0};
// const MediaKeyReport KEY_MEDIA_PREVIOUS_TRACK = {2, 0};
// const MediaKeyReport KEY_MEDIA_STOP = {4, 0};
// const MediaKeyReport KEY_MEDIA_PLAY_PAUSE = {8, 0};
// const MediaKeyReport KEY_MEDIA_MUTE = {16, 0};
// const MediaKeyReport KEY_MEDIA_VOLUME_UP = {32, 0};
// const MediaKeyReport KEY_MEDIA_VOLUME_DOWN = {64, 0};
// const MediaKeyReport KEY_MEDIA_WWW_HOME = {128, 0};
// const MediaKeyReport KEY_MEDIA_LOCAL_MACHINE_BROWSER = {0, 1}; // Opens "My Computer" on Windows
// const MediaKeyReport KEY_MEDIA_CALCULATOR = {0, 2};
// const MediaKeyReport KEY_MEDIA_WWW_BOOKMARKS = {0, 4};
// const MediaKeyReport KEY_MEDIA_WWW_SEARCH = {0, 8};
// const MediaKeyReport KEY_MEDIA_WWW_STOP = {0, 16};
// const MediaKeyReport KEY_MEDIA_WWW_BACK = {0, 32};
// const MediaKeyReport KEY_MEDIA_CONSUMER_CONTROL_CONFIGURATION = {0, 64}; // Media Selection
// const MediaKeyReport KEY_MEDIA_EMAIL_READER = {0, 128};

```

##### ASCII码表

```
ASCII 码表（0-127）
十进制	十六进制	字符	描述
0	0x00	NUL	空字符（Null）
1	0x01	SOH	标题开始（Start of Heading）
2	0x02	STX	文本开始（Start of Text）
3	0x03	ETX	文本结束（End of Text）
4	0x04	EOT	传输结束（End of Transmission）
5	0x05	ENQ	询问（Enquiry）
6	0x06	ACK	确认（Acknowledge）
7	0x07	BEL	响铃（Bell）
8	0x08	BS	退格（Backspace）
9	0x09	HT	水平制表符（Horizontal Tab）
10	0x0A	LF	换行（Line Feed）
11	0x0B	VT	垂直制表符（Vertical Tab）
12	0x0C	FF	换页（Form Feed）
13	0x0D	CR	回车（Carriage Return）
14	0x0E	SO	移出（Shift Out）
15	0x0F	SI	移入（Shift In）
16	0x10	DLE	数据链路转义（Data Link Escape）
17	0x11	DC1	设备控制 1（Device Control 1）
18	0x12	DC2	设备控制 2（Device Control 2）
19	0x13	DC3	设备控制 3（Device Control 3）
20	0x14	DC4	设备控制 4（Device Control 4）
21	0x15	NAK	否定确认（Negative Acknowledge）
22	0x16	SYN	同步空闲（Synchronous Idle）
23	0x17	ETB	传输块结束（End of Transmission Block）
24	0x18	CAN	取消（Cancel）
25	0x19	EM	介质结束（End of Medium）
26	0x1A	SUB	替换（Substitute）
27	0x1B	ESC	转义（Escape）
28	0x1C	FS	文件分隔符（File Separator）
29	0x1D	GS	组分隔符（Group Separator）
30	0x1E	RS	记录分隔符（Record Separator）
31	0x1F	US	单元分隔符（Unit Separator）
32	0x20		空格（Space）
33	0x21	!	感叹号
34	0x22	"	双引号
35	0x23	#	井号
36	0x24	$	美元符号
37	0x25	%	百分号
38	0x26	&	和号
39	0x27	'	单引号
40	0x28	(	左括号
41	0x29	)	右括号
42	0x2A	*	星号
43	0x2B	+	加号
44	0x2C	,	逗号
45	0x2D	-	减号
46	0x2E	.	句号
47	0x2F	/	斜杠
48	0x30	0	数字 0
49	0x31	1	数字 1
50	0x32	2	数字 2
51	0x33	3	数字 3
52	0x34	4	数字 4
53	0x35	5	数字 5
54	0x36	6	数字 6
55	0x37	7	数字 7
56	0x38	8	数字 8
57	0x39	9	数字 9
58	0x3A	:	冒号
59	0x3B	;	分号
60	0x3C	<	小于号
61	0x3D	=	等号
62	0x3E	>	大于号
63	0x3F	?	问号
64	0x40	@	At 符号
65	0x41	A	大写字母 A
66	0x42	B	大写字母 B
67	0x43	C	大写字母 C
68	0x44	D	大写字母 D
69	0x45	E	大写字母 E
70	0x46	F	大写字母 F
71	0x47	G	大写字母 G
72	0x48	H	大写字母 H
73	0x49	I	大写字母 I
74	0x4A	J	大写字母 J
75	0x4B	K	大写字母 K
76	0x4C	L	大写字母 L
77	0x4D	M	大写字母 M
78	0x4E	N	大写字母 N
79	0x4F	O	大写字母 O
80	0x50	P	大写字母 P
81	0x51	Q	大写字母 Q
82	0x52	R	大写字母 R
83	0x53	S	大写字母 S
84	0x54	T	大写字母 T
85	0x55	U	大写字母 U
86	0x56	V	大写字母 V
87	0x57	W	大写字母 W
88	0x58	X	大写字母 X
89	0x59	Y	大写字母 Y
90	0x5A	Z	大写字母 Z
91	0x5B	[	左方括号
92	0x5C	\	反斜杠
93	0x5D	]	右方括号
94	0x5E	^	插入符号
95	0x5F	_	下划线
96	0x60	`	反引号
97	0x61	a	小写字母 a
98	0x62	b	小写字母 b
99	0x63	c	小写字母 c
100	0x64	d	小写字母 d
101	0x65	e	小写字母 e
102	0x66	f	小写字母 f
103	0x67	g	小写字母 g
104	0x68	h	小写字母 h
105	0x69	i	小写字母 i
106	0x6A	j	小写字母 j
107	0x6B	k	小写字母 k
108	0x6C	l	小写字母 l
109	0x6D	m	小写字母 m
110	0x6E	n	小写字母 n
111	0x6F	o	小写字母 o
112	0x70	p	小写字母 p
113	0x71	q	小写字母 q
114	0x72	r	小写字母 r
115	0x73	s	小写字母 s
116	0x74	t	小写字母 t
117	0x75	u	小写字母 u
118	0x76	v	小写字母 v
119	0x77	w	小写字母 w
120	0x78	x	小写字母 x
121	0x79	y	小写字母 y
122	0x7A	z	小写字母 z
123	0x7B	{	左花括号
124	0x7C	`	`	竖线
125	0x7D	}	右花括号
126	0x7E	~	波浪号
127	0x7F	DEL	删除字符（Delete）
```


##### 物物16个点击头控制指令

这部分控制指令和之前上一个版本的wifi点击器一样,使用两个字节16位控制16个点击头的状态,16位从右向左每一个二进制位代表一个点击头状态,0为按下1为抬起,例如:

16个点击头都抬起不按的指令:
```
<FFFF>
```
16个点击头同进都按下的指令:
```
<0000>
```
J1点击头按下:
```
<FFFE>
```
J2点击头按下:
```
<FFFD>
```
J16按下
```
<7FFF>
```
J1和J16同时按下:
```
<7FFE>
```
以下为各个点击头单独按下时的指令值的16进制和二进制表示:
 **点击头编号** | **16进制表示** | **二进制表示**           
-----------|------------|---------------------
 J1        | FFFE       | 1111 1111 1111 1110 
 J2        | FFFD       | 1111 1111 1111 1101 
 J3        | FFFB       | 1111 1111 1111 1011 
 J4        | FFF7       | 1111 1111 1111 0111 
 J5        | FFEF       | 1111 1111 1110 1111 
 J6        | FFDF       | 1111 1111 1101 1111 
 J7        | FFBF       | 1111 1111 1011 1111 
 J8        | FF7F       | 1111 1111 0111 1111 
 J9        | FEFF       | 1111 1110 1111 1111 
 J10       | FDFF       | 1111 1101 1111 1111 
 J11       | FBFF       | 1111 1011 1111 1111 
 J12       | F7FF       | 1111 0111 1111 1111 
 J13       | EFFF       | 1110 1111 1111 1111 
 J14       | DFFF       | 1101 1111 1111 1111 
 J15       | BFFF       | 1011 1111 1111 1111 
 J16       | 7FFF       | 0111 1111 1111 1111 

### 其他命令

串口发送:&

会返回设备mac地址

下边的两个功能目前只在相对坐标固件下完成开发,绝对坐标暂时不支持下边两个命令,后期也会在绝对坐标固件中增加,增加后会自行修改当前markdown文件说明.

串口发送:*

会返回蓝牙名称和一个编号

串口发送:{英文蓝牙名,一个编号}

可以设置蓝牙名,如果手机之前连过个这个蓝牙,蓝牙名称要在手机再次连接成功后才会在手机正确显示新的名称

### 使用示例程序说明

上位机有一个python写的示例程序.主要功能是先控制所有16个物理点击头点击,再控制鼠标到指令点,再控制鼠标在中间从上到下滑动一次.大家要开发功能可以参考


### 蓝牙相对鼠标指令说明

##### 指令格式:

[鼠标按键状态字节,x轴移动最大像素值，x轴移动最大步数，y轴移动最大像素值，y轴移动最大步数，滚轮上下移动最大值，滚轮移动最大步数，滚轮左右移动最大值，滚轮移动最大步数]

//每次接收方括号中9个参数，当没有相应控制时值可以设置为0


一个指令分为五个部分,
第一个部分:

	鼠标按键状态控制
	占一个字节
	鼠标按键一共有5个,左,中,右,向前,向后,对应的字节码分别是:
	//0x01,0x02,0x04,0x08,0x10

第二部分:
	
    鼠标x方向移动控制
    占两个字节
    第一个字节,鼠标移动最大像素值:表示鼠标蓝牙发送移动值的最大值
    第二个字节,鼠标最大值移动步数:表不鼠标移动值从0增加到最大值后保持最大值的发送次数
    
对于移动控制的理解,可以网上搜"相对坐标鼠标移动算法"来理解手机和电脑的相对坐标移动算法是如何计算移动速度和坐标的.这里简单解释一下:

``` Java
//相对坐标的鼠标移动算法相当复杂,微软和苹果都没有开源鼠标移动相关加速和坐标计算算法.只知道目前鼠标的移动是在固定间隔时间向手机或者电脑发送一个加速值,这个值范围在-127到127之前,即一个有符号字节数.我的点击器为了每次发送的数据都产生相同的结果,把计算时间间隔放在了控制板里,所以这里的控制是两个字节.当发送控制指令后,板子会解析出最大移动值,然后在固定8ms的时间里,发关的值从1每次加1直到这个最大值,然后再固定每8ms发送1次最大值,一共发送最大值移动步数次.再慢慢每8ms减1,直到减小到0然后鼠标停下
```
    
第三部分:
	
    鼠标y方向移动控制
    占两个字节
    第一个字节,鼠标移动最大像素值:表示鼠标蓝牙发送移动值的最大值
    第二个字节,鼠标最大值移动步数:表不鼠标移动值从0增加到最大值后保持最大值的发送次数
	
第四部分:
	
    鼠标垂直滚轮移动控制
    占两个字节
    第一个字节,鼠标滚轮最大值:表示鼠标蓝牙发送滚轮值的最大值
    第二个字节,鼠标最大值滚轮步数:表不鼠标滚轮值从0增加到最大值后保持最大值的发送次数
    
第五部分:

	鼠标水平滚轮移动控制(水平方向的滚轮ios好像不支持,所以这两个字节默认都是0x00就好)
    占两个字节
    第一个字节,鼠标滚轮最大值:表示鼠标蓝牙发送滚轮值的最大值
    第二个字节,鼠标最大值滚轮步数:表不鼠标滚轮值从0增加到最大值后保持最大值的发送次数
    
例如:
鼠标向右移动一个距离
```
[000a09000000000000]
```
鼠标向左移动一个距离
```
[00f609000000000000]
```
鼠标按下
```
[010000000000000000]
```
鼠标抬起
```
[000000000000000000]
```
鼠标向右滑动
```
[010a09000000000000]
```
鼠标滚轮向下
```
[00000000000a090000]
```

### 相对坐标实时控制接口

上边的相对坐标是使用主控板里的先加速再减速的方法,下边这里说的是实时控制发送数据间隔的方法

指令如下
```
[0000000000]
```
一共是5个字节的字符表示,

	第一个字节:鼠标按键状态
	第二个字节:鼠标相对坐标中的x下次发送数值,数值范围:-127~+127,注意,同样是使用16进制表示的数
	第三个字节:鼠标相对坐标中的y下次发送数值,数值范围:-127~+127,注意,同样是使用16进制表示的数
	第四个字节:鼠标相对坐标中的鼠标滚轮下次发送数值,数值范围:-127~+127,注意,同样是使用16进制表示的数
    第五个字节:鼠标相对坐标中的鼠标左右方向滚轮下次发送数值,数值范围:-127~+127,注意,同样是使用16进制表示的数

有问题可以加微信:
woodmage


